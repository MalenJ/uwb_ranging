close all


d1 = csvread('exp2_pos1.csv',13,0, [13 0 62 2035]);   %512
componly1=d1(:,4:2035);
compvals1=ones(size(componly1,1),length(componly1)/2);

d2 = csvread('exp2_pos2.csv',13,0, [13 0 62 2035]);
componly2=d2(:,4:2035);
compvals2=ones(size(componly2,1),length(componly2)/2);



%Writing in complex form

for j=1:size(componly1,1)
    
    for i=0:((length(componly1)/2)-1)
    
   compvals1(j,i+1)=complex(componly1(j,i*2+1), componly1(j,i*2+2));
  
    end
end

for j=1:size(componly2,1)
    
    for i=0:((length(componly2)/2)-1)
    
   compvals2(j,i+1)=complex(componly2(j,i*2+1), componly2(j,i*2+2));
  
    end
end


%Obtaining absolute, real and imaginary values 

absvals1=abs(compvals1);
realvals1=real(compvals1);
imagvals1=imag(compvals1);
phasevals1=angle(compvals1); 


absvals2=abs(compvals2);
realvals2=real(compvals2);
imagvals2=imag(compvals2);
phasevals2=angle(compvals2); 



numreads=size(absvals1,1);
ind=numreads/1;


%Processing max values

k=5; %Number of maximum values

maxvals1=ones(numreads,k);
maxindx1=ones(numreads,k);
maxcomps1=ones(numreads,k);
maxphases1=ones(numreads,k);

maxvals2=ones(numreads,k);
maxindx2=ones(numreads,k);
maxcomps2=ones(numreads,k);
maxphases2=ones(numreads,k);


for i=1:numreads
    
    [maxvals1(i,:),maxindx1(i,:)]=maxk(absvals1(i,:), k);
    
    for j=1:k
    
    maxcomps1(i,j)=compvals1(i,maxindx1(i,j));
    maxphases1(i,j)=angle(maxcomps1(i,j));
    
    end
    
end


for i=1:numreads
    
    [maxvals2(i,:),maxindx2(i,:)]=maxk(absvals2(i,:), k);
    
    for j=1:k
    
    maxcomps2(i,j)=compvals2(i,maxindx2(i,j));
    maxphases2(i,j)=angle(maxcomps2(i,j));
    
    end
    
end




[clusterow1, clustercolumn1]=find( maxvals1>=200 & maxvals1<=300 );
size1=size (clusterow1,1);

[clusterow2, clustercolumn2]=find( maxvals2>=400 & maxvals2<=550 );
size2=size (clusterow2,1);

% [clusterow1, clustercolumn1]=find( maxphases1>=0 & maxphases1<=2 );
% size1=size (clusterow1,1);
% 
% [clusterow2, clustercolumn2]=find( maxphases2>=0 & maxphases2<=2 );
% size2=size (clusterow2,1);




figure
for i=1:k
hold on
scatter(d1(:,2),maxvals1(:,i))
scatter(d2(:,2),maxvals2(:,i))
end
 
 for i=1:size1  
   scatter(d1(clusterow1(i),2),maxvals1(clusterow1(i), clustercolumn1(i)),'x', 'k')
 end

 for i=1:size2 
   scatter(d2(clusterow2(i),2),maxvals2(clusterow2(i), clustercolumn2(i)),'x', 'k')
 end
 
 title('distance vs magnitude')


figure
for i=1:k
hold on
scatter(d1(:,2),maxphases1(:,i))
scatter(d2(:,2),maxphases2(:,i))
end

 for i=1:size1  
   scatter(d1(clusterow1(i),2),maxphases1(clusterow1(i), clustercolumn1(i)),'x', 'k')
 end

 for i=1:size2 
   scatter(d2(clusterow2(i),2),maxphases2(clusterow2(i), clustercolumn2(i)),'x', 'k')
 end
 title('distance vs phase')




