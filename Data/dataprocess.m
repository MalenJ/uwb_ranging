
close all


d = csvread('exp6_isolation.csv',17,0, [17 0 27 2035]); %512

componly=d(:,4:2035);

compvals=ones(size(componly,1),length(componly)/2);


%Writing in complex form

for j=1:size(componly,1)
    
    for i=0:((length(componly)/2)-1)
    
   compvals(j,i+1)=complex(componly(j,i*2+1), componly(j,i*2+2));
  
    end
end

%Obtaining absolute, real and imaginary values 

absvals=abs(compvals);
realvals=real(compvals);
imagvals=imag(compvals);
phasevals=angle(compvals); 


%Plotting values
numreads=size(absvals,1);
ind=numreads/1;


figure 
for i=1:ind
hold on
bar(absvals(i,:))
end
title('Absolute vals')

figure
for i=1:ind
hold on
bar(realvals(i,:))
end
 title('Real components')
 
 figure
for i=1:ind
hold on
bar(imagvals(i,:))
end
 title('Imaginary components')
 
 
  figure
for i=1:ind
hold on
bar(phasevals(i,:))
end
 title('Phases')
 
 

%%
%Processing max values

k=100; %Number of maximum values

maxvals=ones(numreads,k);
maxindx=ones(numreads,k);
maxcomps=ones(numreads,k);
maxphases=ones(numreads,k);
outputmat1=ones(numreads, 2*k);
outputmat2=ones(numreads, 2*k);


for i=1:numreads
    
    [maxvals(i,:),maxindx(i,:)]=maxk(absvals(i,:), k);
    out=0;
    
    for j=1:k
    
    maxcomps(i,j)=compvals(i,maxindx(i,j));
    maxphases(i,j)=angle(maxcomps(i,j));
    
    outputmat1(i,2*out+1)=maxvals(i,j);
    outputmat1(i,2*out+2)=maxphases(i,j);
    
    outputmat2(i,2*out+1)=realvals(i,maxindx(i,j));
    outputmat2(i,2*out+2)=imagvals(i,maxindx(i,j));
    out=out+1;
    end
    
end

outputmat1=[d(:,2) outputmat1];
outputmat2=[d(:,2) outputmat2];
%csvwrite('exp3_pos4.csv', outputmat1);
%csvwrite('comp_pos4.csv', outputmat2);


%Plotting

figure 
for i=1:ind
hold on
stem(maxvals(i,:))
end
title('max vals')


figure 
for i=1:ind
hold on
stem(maxphases(i,:))
end
title('max phases')




%Finding min phase

l=100;

minphases=ones(numreads,l);
minindx=ones(numreads,l);

for i=1:numreads
    
   [minphases(i,:),minindx(i,:)]=mink(abs(maxphases(i,:)),l);
    
end
    
figure 
for i=1:ind
hold on
stem(minphases(i,:))
end
title('min phases')





% % minvec=minphases(:,1);
% % meandist=mean(d(:,2))
% % meanphase=mean(minvec)
% % 
% % K=meanphase/meandist




