close all

d = csvread('exp8_pos4_raw.csv',14,0, [14 0 513 2035]); %512
componly=d(:,4:2035);
ranges=d(:,2)/1000;

compvals=ones(size(componly,1),length(componly)/2);


%Writing in complex form

for j=1:size(componly,1)
    
    for i=0:((length(componly)/2)-1)
    
   compvals(j,i+1)=complex(componly(j,i*2+1), componly(j,i*2+2));
  
    end
end

%Obtaining absolute, real and imaginary values 

absvals=abs(compvals);
realvals=real(compvals);
imagvals=imag(compvals);
phasevals=angle(compvals); 


%Plotting values
numreads=size(absvals,1);
ind=numreads/1;


figure 
for i=1:ind
hold on
bar(absvals(i,:))
end
title('Absolute vals')

figure 
plot(ranges)
title('Ranges')


% 
% figure
% for i=1:ind
% hold on
% bar(realvals(i,:))
% end
%  title('Real components')
%  
%  figure
% for i=1:ind
% hold on
% bar(imagvals(i,:))
% end
%  title('Imaginary components')
%  
%  
%   figure
% for i=1:ind
% hold on
% bar(phasevals(i,:))
% end
%  title('Phases')
%  


%% MAX COMPONENT CALC

CHANNEL=4;  % the DecaWave Channel to use (either 2 or 4)
considerSINR=1; % 1...the reflections are limited to those with adequate high signal to noise ratio (SINR)
		    % 0...consider reflections predicted by the environmental model


% set pulse s(t) parameters: raised cosine
% set calibration parameters mean (BIASDECAWAVE, in [1] denotes as \mu) and variance
% (UNCERTDECAWAVE, in [1] denotes as \sigma^2)
%
% Note: the raised cosine parameters were approximated using the observations in Fig. 5 and 6 [1];
% 	the calibration parameters were obtained by measurement which were performed just before the grid measurements
%	were done.
switch CHANNEL
    case 2
        pulse.Tp=2.4e-9; 
        pulse.beta=0.9;
        BIASDECAWAVE=-0.0886; % mean in meter
        UNCERTDECAWAVE=0.0029; % variance in m^2
    case 4
        pulse.Tp=1.5e-9;
        pulse.beta=0.8;
        BIASDECAWAVE=-0.3617; % mean in meter
        UNCERTDECAWAVE=0.00287; % variance in m^2
end

c=3e8;  % propagation speed
OS=30; % oversampling of correlation function of factor 30
CIRlenCUT=201; %limit CIR length to 201 points ~= 60 meters (=201*c/fs_DW, with fs_DW~=1GHz) (to speed up the algorithm)
CIRlenCUT=floor(CIRlenCUT/2)*2+1; % make odd (for simplier calculations)
STARTCIRINBUFFER=700;   % our Pozyx devices saved the channel impulse response at register entry 700 - 1016. 
% This value may change, please check! (to speed up the algorithm)

fs_DW=1/1.0016e-9; % sampling frequency of DecaWave DW1000

pulse.N=CIRlenCUT*OS+1; % save the pulse s(t) with higher sampling rate of 30*fs_DW for more accuracy (~=1cm resolution)
pulse.s = rc(pulse.Tp,pulse.beta,fs_DW*OS,pulse.N); % create raised cosine
pulse.s=(circshift(pulse.s,-(pulse.N+1)/2+1)); % shift rc to minimize linear phase, the maximum is located at index one.

N_Falsi=10; % number of estimated pulses in line-of-sight (LOS) detection
N_Filter=100; % number of sampling points

% Reflections of interest: the number of expected reflections depends on the positions of the anchor,
% agent and the reflection surface locations. We use a ray-tracer to figure
% out the set of possible reflections. The ray-tracer (not included in this paper, the outcome is listed below) 
% does not consider diffractions and penetrations.

% reflections which are expected by the ray-tracer (the numbers assign the index of the reflection surface 
% and differ compared to the paper):
%
% k = 1 ... line-of-sight
%       2 ... plasterboard east
%       7 ... plasterboard west
%       9 ... white board
%       17 ... window
if ~considerSINR
    VAidx=[1,2,7,9,17];
else
% consider only those reflections with high SINR (see Table 1), namely the
% LOS, window and white board
    VAidx=[1,9,17];
end

L_pos=numreads;
IRplot=zeros(CIRlenCUT,L_pos);
absIR=zeros(L_pos,CIRlenCUT);
Extracted=zeros(L_pos,CIRlenCUT);
DDecawave=zeros(L_pos,1);
figure
hold on
% prepare measurements
for l_pos=1:numreads % for each measurement
    cir=circshift(compvals(l_pos,:)',-STARTCIRINBUFFER); % shift channel impulse response to STARTCIRINBUFFER 
                                                           %(Attention: STARTCIRINBUFFER may change using another device)
    cir=cir(1:CIRlenCUT); % limit channel impulse respone (to speed up)
    absCIR=abs(cir);
    Extracted(l_pos,:)=absCIR';
    bar(absCIR)
    
    DDecawave(l_pos)=ranges(l_pos) - BIASDECAWAVE; % consider bias in range estimate
    d=DDecawave(l_pos)/c*fs_DW; % calculate range in samples (rather than in meters)
    % find LOS and return its index using [2]
    LOS_idx=Falsi(cir,pulse.s,N_Falsi,OS); % denoted as t_first in [1]
    % align channel impulse response, shift LOS_idx to DW range estimate d
    linphase=[0,1:(CIRlenCUT-1)/2,-((CIRlenCUT-1)/2:-1:1)]'/CIRlenCUT;
    cir=ifft(fft(cir).*exp(1i*2*pi*(LOS_idx-d)*linphase)); % eq. (6) in [1]
    IRplot(:,l_pos)=cir;
    absIR(l_pos,:)=abs(cir');
end
title('Extracted')

figure 
for i=1:ind
hold on
bar(absIR(i,:))
end
title('Processed')

Extracted=[ranges Extracted];
Processed=[ranges absIR];


csvwrite('exp8_pos4_extract.csv', Extracted);
csvwrite('exp8_pos4_process.csv', Processed);

function s=rc(Tp,beta,fs,N)
% s=rc(Tp,beta,fs,N)
% raised cosine with Tp and beta. Sampling frequency fs in Hertz and number
% of sampling points N
No=floor(N/2)*2+1; % make odd
t=(-(No-1)/2/fs : (1/fs) : (No-1)/2/fs)';
s=sin(pi*t/Tp)./(pi*t/Tp) .* cos(beta*pi*t/Tp)./(1-(2*beta*t/Tp).^2);
s((end-1)/2+1)=1;
s(t==Tp/(2*beta) | t==-Tp/(2*beta))=sin(pi/(2*beta))/(pi/(2*beta))*pi/4;
s=s/sqrt(1/fs*(s'*s)); % normalize energy
end


function [LOS_idx]=Falsi(y,pulse,Nmax,OS)
% LOS_idx=Falsi(y,pulse,Nmax,OS)
% estimate index of line-of-sight using [2]
% y ... signal of interest
% pulse ... waveform to be figured out within y
% Nmax ... number of estimated pulses
% OS ... oversampling of pulse
N=length(y);
x=zeros(length(pulse),1);
x((0:N-1)*OS+1)=y;
E=pulse'*pulse;
psub=zeros(length(pulse),1);
P=fft(flipud(pulse));
c=zeros(Nmax,1);
d=zeros(Nmax,1);
for i=1:Nmax
    xp=ifft((fft(x).*P));
    [~,xind]=max(xp);
    c(i)=xp(xind)/E*OS;
    d(i)=(xind-1)/OS;
    pest=c(i)*circshift(pulse,xind-1);
    psub((0:N-1)*OS+1)=pest((0:N-1)*OS+1);
    x=x-psub;
    psub=zeros(length(pulse),1);
end

LOS_idx=d(find(abs(c)>max(abs(c))/2));
LOS_idx=min(LOS_idx);

end



 