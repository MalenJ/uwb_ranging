#include <FileIO.h>
#include <YunClient.h>
#include <Console.h>
#include <Bridge.h>
#include <Process.h>
#include <Mailbox.h>
#include <BridgeClient.h>
#include <HttpClient.h>
#include <YunServer.h>
#include <BridgeUdp.h>
#include <BridgeSSLClient.h>
#include <BridgeServer.h>

/**
  The Pozyx ready to range tutorial (c) Pozyx Labs
  Please read the tutorial that accompanies this sketch: https://www.pozyx.io/Documentation/Tutorials/ready_to_range/Arduino

  This demo requires two Pozyx devices and one Arduino. It demonstrates the ranging capabilities and the functionality to
  to remotely control a Pozyx device. Place one of the Pozyx shields on the Arduino and upload this sketch. Move around
  with the other Pozyx device.

  This demo measures the range between the two devices. The closer the devices are to each other, the more LEDs will
  light up on both devices.
*/

#include <Pozyx.h>
#include <Pozyx_definitions.h>
#include <Wire.h>

////////////////////////////////////////////////
////////////////// PARAMETERS //////////////////
////////////////////////////////////////////////

uint16_t destination_id = 0x6974;       // the network id of the other pozyx device: fill in the network id of the other device
signed int range_step_mm = 1000;      // every 1000mm in range, one LED less will be giving light.

uint8_t ranging_protocol = POZYX_RANGE_PROTOCOL_PRECISION; // ranging protocol of the Pozyx.

uint16_t remote_id = 0x605D;          // the network ID of the remote device
bool remote = false;                  // whether to use the given remote device for ranging

int channel=0;


uint8_t RegAddCir= 0xC8; 

//uint8_t CIR[5]; 
//uint16_t offset=500;
//uint8_t coefs=1;
//uint8_t params[3];
//int8_t b0, b1, b2, b3, b4, b5;
//int16_t cir0R;
//int16_t cir0I;
//int16_t cir1R;
//int16_t cir1I;
//int16_t cir2R;
//int16_t cir2I;

////////////////////////////////////////////////

void setup(){
  Serial.begin(115200);

  if(Pozyx.begin() == POZYX_FAILURE){
    Serial.println("ERROR: Unable to connect to POZYX shield");
    Serial.println("Reset required");
    delay(100);
    abort();
  }
  
  // setting the remote_id back to NULL will use the local Pozyx
  if (!remote){
    remote_id = NULL;
  }
  Serial.println("------------POZYX RANGING V1.1------------");
  Serial.println("NOTES:");
  Serial.println("- Change the parameters:");
  Serial.println("\tdestination_id (target device)");
  Serial.println("\trange_step (mm)");
  Serial.println();
  Serial.println("- Approach target device to see range and");
  Serial.println("led control");
  Serial.println("------------POZYX RANGING V1.1------------");
  Serial.println();
  Serial.println("START Ranging:");

  // make sure the pozyx system has no control over the LEDs, we're the boss
  uint8_t led_config = 0x0;
  Pozyx.setLedConfig(led_config, remote_id);
  // do the same with the remote device
  Pozyx.setLedConfig(led_config, destination_id);
  // set the ranging protocol
  Pozyx.setRangingProtocol(ranging_protocol, remote_id);
 

//params[2]=coefs;
//*((uint16_t*)(&params[0]))=offset;

//  Serial.print("byte 2= ");
//  Serial.println((int)params[2]);
//  Serial.print("byte 0-1= ");
//  Serial.println((int)(*(uint16_t*)&params[0]));
}

void loop(){
  device_range_t range;
  int status = 0;
  int cirstat= 0;
  int channelstatus=0;

  // let's perform ranging with the destination
  if(!remote)
    status = Pozyx.doRanging(destination_id, &range);
  else
    status = Pozyx.doRemoteRanging(remote_id, destination_id, &range);

//cirstat=Pozyx.regFunction(POZYX_CIR_DATA, params ,3 , CIR, 4);
//if (cirstat==POZYX_SUCCESS){
//     Serial.print("yes, ");
//  } else {
//    Serial.print("no, ");
//  }
//    b0=CIR[0];
//    b1=CIR[1];
//    b2=CIR[2];        
//    b3=CIR[3];
//    b4=CIR[4];
   

uint8_t tster=1;

    
  if (status == POZYX_SUCCESS){
    

    Pozyx.getUWBChannel(&channel, 0);  

     //Serial.print(range.timestamp);
     //Serial.print(",");
     
     //Serial.print("ms, ");
     
     Serial.println(range.distance);
     //Serial.print(",");

    // Serial.print("mm, ");
    
     
     //Serial.print(channel);

     
     //Serial.print(range.RSS);
     
    // Serial.print(",");
     
     
    // Serial.print("dBm,");
     
      //readCIR();
     
//     Serial.print((int)CIR[0]);
//     Serial.print(" b0, ");
//     Serial.print(*((int16_t*)&CIR[0]));
//     Serial.print(" coefReal, ");
//     Serial.print(*((int16_t*)&CIR[2]));
//     Serial.println(" coefImg, ");
//     Serial.print((int)b1);
//     Serial.print((int)b2);
//     Serial.print((int)b3);
//     Serial.print(" b3, ");
//     Serial.print((int)b4);
//     Serial.println(" b4, ");
    
    // now control some LEDs; the closer the two devices are, the more LEDs will be lit
    if (ledControl(range.distance) == POZYX_FAILURE){
      Serial.println("ERROR: setting (remote) leds");
    }
  }
  else{
    tster=0;
    //Serial.println("ERROR: ranging");
  }

  
}

void readCIR(){
  int coff_cnt = 2;
  int offset = 0;
  int i, j, status;
  uint8_t params[3];
  int16_t pData[coff_cnt*2];
  
  params[2] = coff_cnt; // 5 coefficients a time

  int count=0;
  
  for(i = 0; i<508; i++) {
    params[0]= (uint8_t)(coff_cnt*i);
    params[1]= (uint8_t)((coff_cnt*i)>>8);
    status = Pozyx.regFunction(POZYX_CIR_DATA, params, 3, (uint8_t*)pData, coff_cnt*4);
    //delay(10);
    if(status == POZYX_SUCCESS){
      for(j=0; j<coff_cnt; j++) {
        Serial.print(pData[j]);
        Serial.print(",");
        Serial.print(pData[j+1]);
        Serial.print(",");
        count=count+1; 
      }
    } else {
      i--;
    }
  }
  Serial.print(count);
  Serial.println(",");
}

int ledControl(uint32_t range){
  int status = POZYX_SUCCESS;
  // set the LEDs of the pozyx device
  status &= Pozyx.setLed(4, (range < range_step_mm), remote_id);
  status &= Pozyx.setLed(3, (range < 2*range_step_mm), remote_id);
  status &= Pozyx.setLed(2, (range < 3*range_step_mm), remote_id);
  status &= Pozyx.setLed(1, (range < 4*range_step_mm), remote_id);

  // set the LEDs of the destination pozyx device
  status &= Pozyx.setLed(4, (range < range_step_mm), destination_id);
  status &= Pozyx.setLed(3, (range < 2*range_step_mm), destination_id);
  status &= Pozyx.setLed(2, (range < 3*range_step_mm), destination_id);
  status &= Pozyx.setLed(1, (range < 4*range_step_mm), destination_id);

  // status will be zero if setting the LEDs failed somewhere along the way
  return status;
}
