addpath('/home/maleen/git/uwb_ranging/Output/libsvm-3.22/matlab')

%separate labels and data
vMatTrnLbl = trainlabel;
vMatTstLbl = testlabel;

vMatTrnFeat = train;
vMatTstFeat = test;

[n_train,n_cell] = size(vMatTrnFeat);
[n_test,~] = size(vMatTstFeat);

%%%%%%%%%%%%%%%%
%STANDARD SVM
%%%%%%%%%%%%%%%%
disp('Standard SVM');
%Generate the model
tic;
p_time = toc;
vModel = svmtrain(vMatTrnLbl,sparse(vMatTrnFeat),'-s 0 -b 1 -t 0 -g 12 -c 12 -q');
time =  toc - p_time;
outcome = ['Time to generate Model:  ', num2str(time), ' seconds'];
disp(outcome);
p_time =  toc;
%Classify the test data
[Ac1, b, c] = svmpredict(vMatTstLbl,sparse(vMatTstFeat),vModel, ' -b 1');
time =  toc - p_time;
outcome =['Average time per prediction: ', num2str(time/n_test), ' seconds'];
disp(outcome);

%Determine Accuracy
sumsvm = Ac1 - vMatTstLbl;
exact = sum(sumsvm ==0);
outcome = ['Exact:      ', num2str(exact),'/', num2str(n_test), '    ', num2str(exact/n_test*100),'%'];
disp(outcome);
sumsvm = abs(sumsvm);
within1 = sum(sumsvm < 2 | sumsvm>15);
outcome = ['Within 1:   ', num2str(within1),'/', num2str(n_test), '    ', num2str(within1/n_test*100),'%'];
disp(outcome);
disp(' ');

%%
%%%%%%%%%%%%%%%
% SVM WITH PCA
%%%%%%%%%%%%%%%
disp('PCA SVM');

% 
%Calculate how many components required to represent 95% of the training
%data
[pcacoeff,score,latent] = pca(vMatTrnFeat);
coverage = cumsum(latent)./sum(latent);
components = (n_cell - sum(coverage > 0.95)) + 1;
outcome = ['Number of Components: ', num2str(components)];
disp(outcome);400/400

%Transform the test and training data
pcaMatTrnFeat = vMatTrnFeat * pcacoeff(:,1:components);
pcaMatTstFeat = vMatTstFeat * pcacoeff(:,1:components);

%Generate the SVM Model
tic;
p_time =  toc;
vModel = svmtrain(vMatTrnLbl,sparse(pcaMatTrnFeat),'-s 0 -b 1 -t 0 -g 12 -c 12 -q');
time =  toc - p_time;
outcome = ['Time to generate Model:  ', num2str(time), ' seconds'];
disp(outcome);
p_time =  toc;

%Classify the test data
[Ac1, b, c] = svmpredict(vMatTstLbl,sparse(pcaMatTstFeat),vModel, ' -b 1');
time =  toc - p_time;
outcome =['Average time per prediction: ', num2str(time/n_test), ' seconds'];
disp(outcome);

%Determine Accuracy
sumsvm = Ac1 - vMatTstLbl;
exact = sum(sumsvm ==0);
outcome = ['Exact:      ', num2str(exact),'/', num2str(n_test), '    ', num2str(exact/n_test*100),'%'];  
disp(outcome);

sumsvm = abs(sumsvm);
within1 = sum(sumsvm < 2 | sumsvm>15);
outcome = ['Within 1:   ', num2str(within1),'/', num2str(n_test), '    ', num2str(within1/n_test*100),'%'];
disp(outcome);
disp(' ');
