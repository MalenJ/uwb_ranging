exp3pos1=csvread('exp8_pos1_extract.csv');
exp3pos2=csvread('exp8_pos2_extract.csv');
exp3pos3=csvread('exp8_pos3_extract.csv');
exp3pos4=csvread('exp8_pos4_extract.csv');

train1 = [];
train2 = [];
% train3 = [];
% train4 = [];

test1 = [];
test2 = [];
% test3 = [];
% test4 = [];

for i=1:500
    if mod(i,5) ~= 0
        train1(end+1,:) = exp3pos1(i,:);
        train2(end+1,:) = exp3pos2(i,:);
%         train3(end+1,:) = exp3pos3(i,:);
%         train4(end+1,:) = exp3pos4(i,:);
    else 
        test1(end+1,:) = exp3pos1(i,:);
        test2(end+1,:) = exp3pos2(i,:);
%         test3(end+1,:) = exp3pos3(i,:);
%         test4(end+1,:) = exp3pos4(i,:);
    end     
end

% train = [train1; train2; train3; train4];
% test = [test1; test2; test3; test4];
train = [train1; train2;];
test = [test1; test2;];

train = train(:,1:202);
test = test(:,1:202);

% trainlabel=[ones(400,1); ones(400,1)*2; ones(400,1)*3; ones(400,1)*4];
% testlabel=[ones(100,1); ones(100,1)*2; ones(100,1)*3; ones(100,1)*4];
trainlabel=[ones(400,1);ones(400,1)*2];
testlabel=[ones(100,1);ones(100,1)*2];
