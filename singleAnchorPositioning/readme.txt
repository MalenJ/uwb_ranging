*** Supporting material to the ICC ANLN submission entitled ***

Kulmer et al.: Using DecaWave UWB Transceivers for High-accuracy Multipath-assisted Indoor Positioning

*** Included files:
	** filter_singleAnchor.m
		* Matlab script (tested with R2015a) which produces the results shown in Figure 7.
	** room_geometry.mat
		* the 2D room dimensions
	** Pozyx_meas_Ch2.mat
	** Pozyx_meas_Ch4.mat
		* each file consists of 100 measurements of channel impulse responses and ranges between both Pozyx nodes of Channel
			2 (Pozyx_meas_Ch2.mat) and 4 (Pozyx_meas_Ch4.mat).

*** A note on the measurements
	We used the Pozyx platform (https://www.pozyx.io) which embeds the DecaWave DW1000 UWB chip. We accessed the channel impulse
	response (CIR) using the function "POZYX_CIR_DATA" (see www.pozyx.io/Documentation/Datasheet/RegisterOverview) to return the
	complex valued CIR with a length of 1016 values (using a pulse repetition frequency of 64MHz). The range estimates are per-
	formed by Pozyx using "POZYX_DEVICE_GETRANGEINFO".
	An easy accessible alternative to the Pozyx platform are the evaluation kits by DecaWave 
	(http://www.decawave.com/products/evk1000-evaluation-kit) which drive the DW1000. The provided software DecaRanging is capable
	to return the CIR similar to the Pozyx platform. A two-way ranging algorithm is also provided.
	

*** Graz, December 2016
