function filter_singleAnchor(CHANNEL,considerSINR)
% filter_singleAnchor(CHANNEL,considerSINR)
%
% Position estimation of one agent using time-of-arrival estimates of
% multipath components.
% 
% The following script accomponies the submission:
% [1] Kulmer et al.: Using DecaWave UWB transceiver for high-accuracy multipath-assisted indoor positioning
%	submitted to ICC-ANLN 2017
%
% further references:
% [2] Falsi et al.: Time of Arrival Estimation for UWB Localizers in Realistic Environments, 
%       EURASIP Journal on Applied Signal Processing, 2006
% [3] Kulmer et al.: Cooperative Localization and Tracking using Multipath Channel Information, GNSS 2016
%
% tested with Matlab R2015a
%
% Graz, Dezember 2016

if nargin==0
    CHANNEL=4;  % the DecaWave Channel to use (either 2 or 4)
    considerSINR=1; % 1...the reflections are limited to those with adequate high signal to noise ratio (SINR)
		    % 0...consider reflections predicted by the environmental model
end

% set pulse s(t) parameters: raised cosine
% set calibration parameters mean (BIASDECAWAVE, in [1] denotes as \mu) and variance
% (UNCERTDECAWAVE, in [1] denotes as \sigma^2)
%
% Note: the raised cosine parameters were approximated using the observations in Fig. 5 and 6 [1];
% 	the calibration parameters were obtained by measurement which were performed just before the grid measurements
%	were done.
switch CHANNEL
    case 2
        pulse.Tp=2.4e-9; 
        pulse.beta=0.9;
        BIASDECAWAVE=-0.0886; % mean in meter
        UNCERTDECAWAVE=0.0029; % variance in m^2
    case 4
        pulse.Tp=1.5e-9;
        pulse.beta=0.8;
        BIASDECAWAVE=-0.3617; % mean in meter
        UNCERTDECAWAVE=0.00287; % variance in m^2
end

c=3e8;  % propagation speed
OS=30; % oversampling of correlation function of factor 30
CIRlenCUT=201; %limit CIR length to 201 points ~= 60 meters (=201*c/fs_DW, with fs_DW~=1GHz) (to speed up the algorithm)
CIRlenCUT=floor(CIRlenCUT/2)*2+1; % make odd (for simplier calculations)
STARTCIRINBUFFER=700;   % our Pozyx devices saved the channel impulse response at register entry 700 - 1016. 
% This value may change, please check! (to speed up the algorithm)

fs_DW=1/1.0016e-9; % sampling frequency of DecaWave DW1000

pulse.N=CIRlenCUT*OS+1; % save the pulse s(t) with higher sampling rate of 30*fs_DW for more accuracy (~=1cm resolution)
pulse.s = rc(pulse.Tp,pulse.beta,fs_DW*OS,pulse.N); % create raised cosine
pulse.s=(circshift(pulse.s,-(pulse.N+1)/2+1)); % shift rc to minimize linear phase, the maximum is located at index one.

N_Falsi=10; % number of estimated pulses in line-of-sight (LOS) detection
N_Filter=100; % number of sampling points

% Reflections of interest: the number of expected reflections depends on the positions of the anchor,
% agent and the reflection surface locations. We use a ray-tracer to figure
% out the set of possible reflections. The ray-tracer (not included in this paper, the outcome is listed below) 
% does not consider diffractions and penetrations.

% reflections which are expected by the ray-tracer (the numbers assign the index of the reflection surface 
% and differ compared to the paper):
%
% k = 1 ... line-of-sight
%       2 ... plasterboard east
%       7 ... plasterboard west
%       9 ... white board
%       17 ... window
if ~considerSINR
    VAidx=[1,2,7,9,17];
else
% consider only those reflections with high SINR (see Table 1), namely the
% LOS, window and white board
    VAidx=[1,9,17];
end

% the rest of the script is divided in to parts, first, load the
% measurements and prepare the channel impulse responses, and second,
% perform the positioning

% load the room shape (denoted as S)and the position of the basestation
% \mathbf{a} (denoted as BS)
load('room_geometry.mat')
% calculate the virtual anchor positions using the room geometry and the
% basestation (see [3])
R=[0,-1;1,0];
VA=zeros(2,size(S,1)+1);
VA(:,1)=BS;
for iS=1:size(S,1);
    Stemp=S(iS,:);
    etemp=[Stemp(3)-Stemp(1);Stemp(4)-Stemp(2)];
    e=R*etemp/norm(etemp);    % e_s'
    Rs=e*e';
    ps=S(iS,1:2)';
    VA(:,iS+1)=(eye(2)-2*Rs)*BS+2*Rs*ps; % Eq. (3) in [3]
end

% load Pozyx files:
%   DW_meas ... a cell which contains one measure impulse response DW_meas{.}.IR and
%       corresponing range estimate DW_meas{.}.d
%   p ... the position of the receiver \mathbf{p}_n of each measurement
load(['Pozyx_meas_Ch',num2str(CHANNEL)]);
L_pos=length(Pozyx_meas); % number of measurements
IRplot=zeros(CIRlenCUT,L_pos);
DDecawave=zeros(L_pos,1);

% prepare measurements
for l_pos=1:length(Pozyx_meas) % for each measurement
    cir=circshift(Pozyx_meas{l_pos}.IR,-STARTCIRINBUFFER); % shift channel impulse response to STARTCIRINBUFFER 
                                                           %(Attention: STARTCIRINBUFFER may change using another device)
    cir=cir(1:CIRlenCUT); % limit channel impulse respone (to speed up)
    DDecawave(l_pos)=Pozyx_meas{l_pos}.d - BIASDECAWAVE; % consider bias in range estimate
    d=DDecawave(l_pos)/c*fs_DW; % calculate range in samples (rather than in meters)
    % find LOS and return its index using [2]
    LOS_idx=Falsi(cir,pulse.s,N_Falsi,OS); % denoted as t_first in [1]
    % align channel impulse response, shift LOS_idx to DW range estimate d
    linphase=[0,1:(CIRlenCUT-1)/2,-((CIRlenCUT-1)/2:-1:1)]'/CIRlenCUT;
    cir=ifft(fft(cir).*exp(1i*2*pi*(LOS_idx-d)*linphase)); % eq. (6) in [1]
    IRplot(:,l_pos)=cir;
end

% perform filtering

% limit possible set of solutions to be inside the room, only a rough guess
S11=min(S(:,1));
S12=max(S(:,1));
S21=min(S(:,2));
S22=max(S(:,2));

phat=zeros(L_pos,2);
for l_pos=1:L_pos % for each measurement
    ir=IRplot(:,l_pos);
    x=zeros(N_Filter,2);
    iN=1;
    % calculate the set of sampling points
    while iN<=N_Filter
        randphi=rand*2*pi; % random angle
        randamp=randn*sqrt(UNCERTDECAWAVE); % consider uncertainty of DecaWave range estimate
        xtemp=(DDecawave(l_pos)+randamp)*[cos(randphi),sin(randphi)]+BS'; % put sampling point on a circle around the base station
        % reject sampling point if it is outside the room
        if (xtemp(1)>S11 && xtemp(1)<S12 && xtemp(2)>S21 && xtemp(2)<S22)
            x(iN,:)=xtemp;
            iN=iN+1;
        end
    end
    
    w=zeros(N_Filter,1);
    for iN=1:N_Filter % for each sampling point
        w(iN)=0;
        % signal matrix
        Dvis=NaN(length(VAidx),1);
        % for each multipath component, calculate expected distance from
        % sampling point to virtual anchor
        for iVA=1:length(VAidx) % LOS (1), PB east (2), PB west (7), Whiteboard (9), window (17)
            Dvis(iVA)=norm(x(iN,:)'-VA(:,VAidx(iVA))); % eq. (2) in [1]
        end
        signalmtx=zeros(CIRlenCUT,length(VAidx));
        % construct signal matrix S(\tau) (eq. (9) in [1])
        for iVA=1:length(VAidx)
            spulse=circshift(pulse.s,round(Dvis(iVA)/c*fs_DW*OS)); % align pulse to expected distance
            signalmtx(:,iVA)=spulse((0:CIRlenCUT-1)*OS+1); % downsampling
        end
        alphatemp=pinv(signalmtx'*signalmtx)*signalmtx'*ir; % least-squares solution to get amplitudes 
							    %    (the equation between (9) and (10) in [1])
        w(iN)=w(iN)-1/2*(ir-signalmtx*alphatemp)'*(ir-signalmtx*alphatemp); % calculate likelihood (eq. (10) in [1])
    end
    [~,w_idx]=max(w);   % maximum of likelihood (eq. (11) in [1])
    phat(l_pos,1:2)=x(w_idx,1:2); % set \hat{\mathbf{p}} to sampling point which maximizes the likelihood
    % plot
    figure(100)
    plot(p(1,l_pos),p(2,l_pos),'rx')
    hold on
    plot(phat(l_pos,1),phat(l_pos,2),'ko')
    scatter3(x(:,1),x(:,2),w,30,w,'filled')
    plot(S(:,[1,3])',S(:,[2,4])','k')
    plot(BS(1),BS(2),'bx')
    hold off
    xlabel('x dimension in meter')
    ylabel('y dimension in meter')
    drawnow
end
figure
temp=phat'-p;
plot(sort(sqrt(diag(temp'*temp))),(0:(L_pos-1))/L_pos)
xlabel('error in meter')
ylabel('cumulative distribution function')

function s=rc(Tp,beta,fs,N)
% s=rc(Tp,beta,fs,N)
% raised cosine with Tp and beta. Sampling frequency fs in Hertz and number
% of sampling points N
No=floor(N/2)*2+1; % make odd
t=(-(No-1)/2/fs : (1/fs) : (No-1)/2/fs)';
s=sin(pi*t/Tp)./(pi*t/Tp) .* cos(beta*pi*t/Tp)./(1-(2*beta*t/Tp).^2);
s((end-1)/2+1)=1;
s(t==Tp/(2*beta) | t==-Tp/(2*beta))=sin(pi/(2*beta))/(pi/(2*beta))*pi/4;
s=s/sqrt(1/fs*(s'*s)); % normalize energy

function [LOS_idx]=Falsi(y,pulse,Nmax,OS)
% LOS_idx=Falsi(y,pulse,Nmax,OS)
% estimate index of line-of-sight using [2]
% y ... signal of interest
% pulse ... waveform to be figured out within y
% Nmax ... number of estimated pulses
% OS ... oversampling of pulse
N=length(y);
x=zeros(length(pulse),1);
x((0:N-1)*OS+1)=y;
E=pulse'*pulse;
psub=zeros(length(pulse),1);
P=fft(flipud(pulse));
c=zeros(Nmax,1);
d=zeros(Nmax,1);
for i=1:Nmax
    xp=ifft((fft(x).*P));
    [~,xind]=max(xp);
    c(i)=xp(xind)/E*OS;
    d(i)=(xind-1)/OS;
    pest=c(i)*circshift(pulse,xind-1);
    psub((0:N-1)*OS+1)=pest((0:N-1)*OS+1);
    x=x-psub;
    psub=zeros(length(pulse),1);
end

LOS_idx=d(find(abs(c)>max(abs(c))/2));
LOS_idx=min(LOS_idx);
